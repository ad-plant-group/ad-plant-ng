import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContestPage } from './contest.page';

describe('ContestPage', () => {
  let component: ContestPage;
  let fixture: ComponentFixture<ContestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
