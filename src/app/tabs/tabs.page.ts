import { Component, ViewChild } from '@angular/core';
import { IonTabs } from "@ionic/angular";

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage {

    @ViewChild('tabsSelector', {static: false}) tabsSelector: IonTabs;

    constructor() {
    }

    /**
     * active le tab pour les donations
     */
    public activateTabDonate(): void {


        setTimeout(() => this.tabsSelector.select('donate').then(), 0);

    }
}
