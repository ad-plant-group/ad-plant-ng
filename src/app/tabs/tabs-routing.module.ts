import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'ads',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../page/ads/ads.module').then(m => m.AdsPageModule)
                    }
                ]
            },
            {
                path: 'contest',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../page/contest/contest.module').then(m => m.ContestPageModule)
                    }
                ]
            },
            {
                path: 'donate',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../page/donate/donate.module').then(m => m.DonatePageModule)
                    }
                ]
            },
            {
                path: 'account',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../page/account/account.module').then(m => m.AccountPageModule)
                    }
                ]
            },
            {
                path: 'settings',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../page/settings/settings.module').then(m => m.SettingsPageModule)
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/donate',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/ads',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
