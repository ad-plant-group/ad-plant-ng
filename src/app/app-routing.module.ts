import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'ads', loadChildren: './page/ads/ads.module#AdsPageModule' },
  { path: 'contest', loadChildren: './page/contest/contest.module#ContestPageModule' },
  { path: 'donate', loadChildren: './page/donate/donate.module#DonatePageModule' },
  { path: 'account', loadChildren: './page/account/account.module#AccountPageModule' },
  { path: 'settings', loadChildren: './page/settings/settings.module#SettingsPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
