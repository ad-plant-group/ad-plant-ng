# Ad Plant App
This app allows individuals and building professionals to get rid of bulky metal without having to go to dump!

And for recycling professionals, recover scrap without having to run all day since the ads are geolocated.

And in addition you participate ALL thanks to our partners to replant trees everywhere in France!

You can also contribute by donating (without spending a dime) by viewing 3 videos of our partner daily and you participate in replanting trees in the French forests!
### Frameworks versions

| Framework | Version | Links |
| ------- | ------- |:-----:|
| **Ionic**  | ![version](https://img.shields.io/badge/ionic-v5.4.5-blue) | [`Ionic Docs`](https://ionicframework.com/docs/cli/)
| **Angular** | ![version](https://img.shields.io/badge/angular-v8.1.3-blue) | [`Angular Docs`](https://angular.io/docs)


## Dev Requirements :rotating_light:
- npm
- node v10.9+
- git

## Getting started

This project was generated with Ionic.

### Clone the project 📥

Please launch this command to clone the project :

`git clone https://gitlab.com/ad-plant-group/ad-plant-ng.git`

### Install dependencies 

Use npm to install all dependencies needed : 

`npm i`

### Run app

Change directory to `cd ad-plant-ng`

Run the app : `ionic serve`


## Usage

###Code scaffolding
For automatically create framework features use  ionic g :

`ionic g page|component|directive|service NAME`
    
### Theming the app

see : https://ionicframework.com/docs/theming/color-generator

### Further help

To get more help on Ionic use `ionic help` or go check out [Ionic README](https://github.com/ionic-team/ionic/blob/master/README.md)
